package com.example.reactwithreduxAPI;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class LikeController {
    @RequestMapping(value = "/like")
    @ResponseStatus(HttpStatus.CREATED)
    public void like(@RequestBody UserProfileLike like) {
        System.out.println("==============");
        System.out.println(String.format("User Profile %s has been liked by %s",
                String.valueOf(like.getUserProfileId()),
                like.getUserName()));
        System.out.println("==============");
    }
}
